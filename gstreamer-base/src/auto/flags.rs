// This file was generated by gir (https://github.com/gtk-rs/gir)
// from gir-files (https://github.com/gtk-rs/gir-files)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git)
// DO NOT EDIT

use bitflags::bitflags;
use glib::translate::*;

bitflags! {
    pub struct BaseParseFrameFlags: u32 {
        const NEW_FRAME = 1;
        const NO_FRAME = 2;
        const CLIP = 4;
        const DROP = 8;
        const QUEUE = 16;
    }
}

#[doc(hidden)]
impl IntoGlib for BaseParseFrameFlags {
    type GlibType = ffi::GstBaseParseFrameFlags;

    fn into_glib(self) -> ffi::GstBaseParseFrameFlags {
        self.bits()
    }
}

#[doc(hidden)]
impl FromGlib<ffi::GstBaseParseFrameFlags> for BaseParseFrameFlags {
    unsafe fn from_glib(value: ffi::GstBaseParseFrameFlags) -> Self {
        skip_assert_initialized!();
        Self::from_bits_truncate(value)
    }
}
